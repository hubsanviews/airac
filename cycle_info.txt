AIRAC cycle    : 1906
Version        : 1
Valid (from/to): 23/MAY/2019 - 20/JUN/2019

Forum          : http://forum.navigraph.com

Data provided by Navigraph - www.navigraph.com - Source data copyright (c) 2019 Jeppesen
This data may be used for ground based recreational computer game
simulation software only, and may not be recompiled, interpreted,
or distributed for any purpose without the written consent of Navigraph.
The contents of this database is dated and must not be used for real
world navigation as it is unlawful and unsafe to do so.

Parser-Version : DFD v1.0 19.0516 (c) Richard Stefan
Files parsed on: 16/05/2019
